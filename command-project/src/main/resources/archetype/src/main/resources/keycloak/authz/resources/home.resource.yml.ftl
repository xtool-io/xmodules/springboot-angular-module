---
resource:
  clientId: ${projectName}-backend
  name: "home:resource"
  uris: ["/home"]
  scopes: ["view"]
