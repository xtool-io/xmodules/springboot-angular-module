---
client:
  name: ${projectName}-frontend
  redirectUris: ["/${projectName}-frontend/*"]
  webOrigins: ["/app2-frontend/*"]
  baseUrl: /${projectName}-frontend
  publicClient: true
  directAccessGrantsEnabled: true
  standardFlowEnabled: true
  protocolMappers:
    - name: group_mapper
      type: oidc-group-membership-mapper
      protocol: openid-connect
      config:
        claim.name: groups
        full.path: false
        id.token.claim: false
        access.token.claim: true
        userinfo.token.claim: false
  scopeMappings:
    - client: "account"
      roles: ["view-profile", "manage-account"]
    - client: "${projectName}-backend"
      allRoles: true
