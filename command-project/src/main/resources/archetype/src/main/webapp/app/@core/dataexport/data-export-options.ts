import {DataExportType} from "./data-export-type.enum";
import {DxiDataGridColumn} from "devextreme-angular/ui/nested/base/data-grid-column-dxi";

export class DataExportOptions {
  constructor(
    exportType: DataExportType = DataExportType.CSV,
    columns: string[]) {
  }

}
