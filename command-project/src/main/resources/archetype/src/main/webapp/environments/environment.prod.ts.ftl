export const environment = {
  production: true,
  contextPath: '/${projectName}',
  keycloak: {
    config: {
      url: '/auth',
      clientId: '${projectName}-frontend',
      resourceId: '${projectName}-backend',
      realm: 'TRE-PA'
    },
    initOptions: {
      onLoad: 'login-required',
      checkLoginIframe: true,
      enableLogging: false
    }
  }
};
