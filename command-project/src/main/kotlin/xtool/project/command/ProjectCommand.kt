package xtool.project.command

import org.springframework.stereotype.Component
import picocli.CommandLine.Command
import xtool.project.command.subcommand.InitProjectCommand

@Component
@Command(name = "project", subcommands = [InitProjectCommand::class])
class ProjectCommand : Runnable {

    override fun run() {

    }

}
