package xtool.project.command.subcommand

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import picocli.CommandLine
import picocli.CommandLine.Command
import xdeps.xdepfs.context.CopyFileContext
import xdeps.xdepfs.context.WorkspaceContext
import xdeps.xdepfs.tasks.FileSystemTask
import xdeps.xdepfs.tasks.ShellTask

@Component
@Command(name = "init")
class InitProjectCommand(
    private val workspaceContext: WorkspaceContext,
    private val fsTask: FileSystemTask,
    private val shellTask: ShellTask,
) : Runnable {

    private val log = LoggerFactory.getLogger(InitProjectCommand::class.java)

    @CommandLine.Option(names = ["--description"], description = ["Descrição do projeto"], required = true)
    lateinit var projectDescription: String
    override fun run() {
        require(workspaceContext.isEmpty) { "Para a criação de um projeto o diretório de trabalho deve estar vazio." }
//        header("Copiando arquivos do projeto")
        fsTask.copyFiles(
            classpathSource = "archetype",
            destination = workspaceContext.path,
            vars = mapOf(
                "projectName" to workspaceContext.basename,
                "projectDesc" to projectDescription
            ),
            onEachCreated = { ctx: CopyFileContext -> log.info("${ctx.path} (${ctx.byteCountToDisplaySize})") }
        )
//        header("Baixando dependências do frontend")
//        npmTask.install()
//        header("Versionando projeto")
        shellTask.runCommand(
            command = """
                git init &&
                git add . &&
                git commit -m "First Commit"
            """.trimIndent()
        )
    }
}
