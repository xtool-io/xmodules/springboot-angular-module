package service

import org.junit.jupiter.api.Test
import xdeps.xdepfs.context.WorkspaceContext
import xdeps.xdepjava.tasks.JavaTask
import xdeps.xdepplantuml.tasks.PlantTask
import xtool.plantuml.service.GenerateSpringBootEntityFromPlantUmlService
import java.io.File
import java.nio.file.Paths

class GenerateSpringBootEntityFromPlantUmlServiceTestCase {

    @Test
    fun createClasses() {
        File("/tmp/br").deleteRecursively()
        val workspaceContext = WorkspaceContext(
            path = Paths.get("/tmp")
        )
        val generateSpringBootEntityFromPlantUmlService = GenerateSpringBootEntityFromPlantUmlService(
            workspaceContext,
            PlantTask(),
            JavaTask()
        )
        generateSpringBootEntityFromPlantUmlService.exec(
            mapOf(
                "path" to Paths.get("src/test/resources/domain.plantuml"),
                "domainPath" to Paths.get("br/jus/tre_pa/app/domain")
            )
        )
    }
}
