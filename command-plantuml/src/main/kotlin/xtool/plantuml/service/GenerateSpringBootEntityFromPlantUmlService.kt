package xtool.plantuml.service

import lombok.EqualsAndHashCode
import lombok.Getter
import lombok.NoArgsConstructor
import lombok.Setter
import org.jboss.forge.roaster.model.source.FieldSource
import org.jboss.forge.roaster.model.source.JavaClassSource
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import xdeps.xdepfs.context.WorkspaceContext
import xdeps.xdepjava.tasks.JavaTask
import xdeps.xdepplantuml.model.PlantClass
import xdeps.xdepplantuml.model.PlantField
import xdeps.xdepplantuml.model.PlantRelationship
import xdeps.xdepplantuml.tasks.PlantTask
import xtool.plantuml.utils.JavaClass
import xtool.plantuml.utils.javaTypeClassOf
import java.nio.file.Path
import javax.persistence.*
import javax.validation.constraints.Size


typealias JavaField = FieldSource<JavaClassSource>

/**
 * Classe de serviço responsável pela geração das entidades JPA a partir de um diagrama de classe plantUML.
 */

@Service
class GenerateSpringBootEntityFromPlantUmlService(
    private val workspaceContext: WorkspaceContext,
    private val plantTask: PlantTask,
    private val javaTask: JavaTask,
) {

    private val log = LoggerFactory.getLogger(GenerateSpringBootEntityFromPlantUmlService::class.java)

    fun exec(args: Map<String, Any>) {
        val path = args["path"] as Path
        val domainPath = args["domainPath"] as Path
        val plantClassDiagram = plantTask.readClassDiagram(path)
        log.info("{} classes encontradas no diagrama {}", plantClassDiagram.classes.size, path)
        // Itera pelas classes do diagrama
        plantClassDiagram.classes.forEach { plantClass ->
            // Cria o objeto JavaClass a partir da classe do diagrama
            createJavaClassFrom(plantClass).let { javaClass ->
                // Adiciona a annotation @Entity à classe
                addEntityAnnotation(javaClass)
                // Adiciona as annotations do Lombok à classe
                addLombokAnnotations(javaClass)
                // Itera pelos atributos da classe plantUML
                plantClass.attributes.forEach { plantField ->
                    // Adiciona o atributo a classe
                    createField(plantField, javaClass).let { javaField ->
                        // Adiciona as annotations ao atributo Id
                        addIdAnnotations(plantField, javaField, javaClass)
                        // Adiciona a annotation @Column
                        addColumnAnnotation(plantField, javaField, javaClass)
                        // Adiciona a annotation @Size
                        addSizeAnnotation(plantField, javaField, javaClass)
                    }
                }
                writeClass(domainPath, javaClass)
                plantClassDiagram.relationships
                    .onEach { warnNonUnidirectionalRelationship(it) }
                    .filter { listOf(it.plantClasses.first.name, it.plantClasses.second.name).contains(plantClass.name) }
                    .forEach {
                        println("${it.plantClasses.first.name}, ${it.plantClasses.second.name}")
                    }
            }
        }
    }

    fun createJavaClassFrom(
        plantClass: PlantClass
    ): JavaClass {
        return javaTask.createClass(plantClass.name, plantClass.packageName ?: "")
    }

    fun writeClass(domainPath: Path, javaClass: JavaClass) {
        val entityPath = workspaceContext.path.resolve(domainPath).resolve("${javaClass.name}.java")
        javaTask.writeClass(entityPath, javaClass)
        log.info("{}", entityPath)
    }

    fun addEntityAnnotation(javaClass: JavaClass) {
        javaTask.addClassAnnotation(javaClass, Entity::class.java)
    }

    // Adiciona as annotation do Lombok à entidade
    fun addLombokAnnotations(javaClass: JavaClass) {
        javaTask.addClassAnnotation(javaClass, Getter::class.java)
        javaTask.addClassAnnotation(javaClass, Setter::class.java)
        javaTask.addClassAnnotation(javaClass, NoArgsConstructor::class.java)
        javaTask.addClassAnnotation(javaClass, EqualsAndHashCode::class.java) { it.setStringValue("of", "id") }
    }


    fun createField(plantField: PlantField, javaClass: JavaClass): JavaField {
        return javaTask.addField(javaClass, plantField.name) {
            it.setType(javaTypeClassOf(plantField.type))
        }
    }


    //Adiciona as annotation para o atributo Id
    fun addIdAnnotations(plantField: PlantField, javaField: JavaField, javaClass: JavaClass) {
        if (plantTask.isId(plantField)) {
            javaTask.addFieldAnnotation(javaField, Id::class.java)
            javaTask.addFieldAnnotation(javaField, GeneratedValue::class.java) {
                it.setEnumValue("strategy", GenerationType.SEQUENCE)
                it.setStringValue("generator", "SEQ_${javaClass.name.take(26).uppercase()}")
            }
            javaTask.addFieldAnnotation(javaField, SequenceGenerator::class.java) {
                it.setLiteralValue("initialValue", "1")
                it.setLiteralValue("allocationSize", "1")
                it.setStringValue("name", "SEQ_${javaClass.name.take(26).uppercase()}")
            }
        }
    }

    fun addColumnAnnotation(plantField: PlantField, javaField: JavaField, javaClass: JavaClass) {
        // Apenas aplica a annotation @Column caso o atributo não seja Id.
        if (!plantTask.isId(plantField)) {
            javaTask.addFieldAnnotation(javaField, Column::class.java) {
                if (plantTask.containsProperty(plantField, "unique")) it.setLiteralValue("unique", "true")
                if (plantTask.containsProperty(plantField, "nullable")) it.setLiteralValue("nullable", "false")
                // Adiciona o atributo length a annotation
                if (plantField.type == "String") {
                    if (plantTask.hasMultiplicity(plantField)) {
                        it.setLiteralValue("length", plantField.multiplicity)
                    }
                }
            }
        }
    }

    fun addSizeAnnotation(plantField: PlantField, javaField: JavaField, javaClass: JavaClass) {
        if (!plantTask.isId(plantField)) {
            if (plantTask.hasMultiplicity(plantField)) {
                javaTask.addFieldAnnotation(javaField, Size::class.java) {
                    it.setLiteralValue("max", plantField.multiplicity)
                }
            }
        }
    }

    /**
     * Adiciona um relacionamento OneToMany
     */
    fun addOneToManyRelationship(plantRelationship: PlantRelationship, javaClass: JavaClass) {
    }

    /**
     * Exibe um alerta caso o relacionamento não seja unidirecional.
     */
    private fun warnNonUnidirectionalRelationship(plantRelationship: PlantRelationship) {
        if (plantRelationship.isAssociation) {
            if (plantRelationship.navigabilities.first xor plantRelationship.navigabilities.second) {
                log.warn(
                    "Só são permitidos relacionamentos unidirecionais entre as classes. Verifique o relacionamento entre {} e {}",
                    plantRelationship.plantClasses.first.name,
                    plantRelationship.plantClasses.second.name
                )
            }
        }
    }

}
