package xtool.plantuml.command

import org.springframework.stereotype.Component
import picocli.CommandLine.Command

@Component
@Command(name = "plantuml")
class PlantUmlCommand : Runnable {
    override fun run() {
    }

    @Command(name = "generate-entities")
    fun generateJpaEntitiesCommand() {
        println("Gerando entidades JPA...via fn")
    }

}
