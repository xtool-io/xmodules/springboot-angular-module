package xtool

import org.fusesource.jansi.Ansi
import org.fusesource.jansi.AnsiConsole
import org.jline.console.impl.SystemRegistryImpl
import org.jline.reader.*
import org.jline.reader.impl.DefaultParser
import org.jline.terminal.TerminalBuilder
import org.jline.widget.AutosuggestionWidgets
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.ExitCodeGenerator
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.stereotype.Component
import picocli.CommandLine
import picocli.CommandLine.Command
import picocli.shell.jline3.PicocliCommands
import xtool.plantuml.command.PlantUmlCommand
import xtool.project.command.ProjectCommand
import java.nio.file.Paths
import java.util.function.Supplier

@Component
@Command(name = "", subcommands = [ProjectCommand::class, PlantUmlCommand::class])
class MainCommand : Runnable {
    override fun run() {
    }
}

@SpringBootApplication
class SpringBootAngularModule(
    private var factory: CommandLine.IFactory,
    private var mainCommand: MainCommand,
) : CommandLineRunner, ExitCodeGenerator {
    private var exitCode = 0
    override fun run(vararg args: String?) {
        AnsiConsole.systemInstall();
        try {
//            println("""Módulo: ${Ansi.ansi().fgBrightCyan().bold().a(env.getProperty("module.name")).a("@").a(env.getProperty("module.version")).reset()}""")
//            println("")
            val workDir = Paths.get(System.getProperty("user.dir"))
            val pathSupplier = Supplier { workDir }
            // let picocli parse command line args and run the business logic
            val cmd = CommandLine(mainCommand, factory)
            val picocliCommands = PicocliCommands(cmd)
            val parser = DefaultParser()
            val terminal = TerminalBuilder.builder().build()
            val systemRegistry = SystemRegistryImpl(parser, terminal, pathSupplier, null)
            systemRegistry.setCommandRegistries(picocliCommands)
            systemRegistry.register("help", picocliCommands)
            val reader = LineReaderBuilder.builder()
                .terminal(terminal)
                .completer(systemRegistry.completer())
                .parser(parser)
                .variable(LineReader.LIST_MAX, 50) // max tab completion candidates
                .build()
            val widgets = AutosuggestionWidgets(reader)
            widgets.enable()
            val prompt = Ansi.ansi().bold().fgBrightGreen().a("xctl@${workDir.toFile().name}# ").reset().toString()
            val rightPrompt = null
            var line: String?
            while (true) {
                try {
                    systemRegistry.cleanUp()
                    line = reader.readLine(prompt, rightPrompt, null as MaskingCallback?, null)
                    systemRegistry.execute(line)
                } catch (e: UserInterruptException) {
                    // Ignore
                } catch (e: EndOfFileException) {
                    return
                } catch (e: Exception) {
                    systemRegistry.trace(e)
                }
            }
        } catch (t: Throwable) {
            t.printStackTrace();
        } finally {
            AnsiConsole.systemUninstall();
        }
    }

    override fun getExitCode(): Int {
        return exitCode
    }


}

fun main(args: Array<String>) {
    SpringApplicationBuilder(SpringBootAngularModule::class.java)
        .logStartupInfo(false)
        .headless(false)
        .run(*args)
}
